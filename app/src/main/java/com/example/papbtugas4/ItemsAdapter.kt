package com.example.papbtugas4
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ItemsAdapter(private val context: Context, private val items: List<Items>, val listener: (Items) ->Unit)
    : RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder>() {

    class ItemsViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val imageHero = view.findViewById<ImageView>(R.id.img_item)
        val nameHeroes = view.findViewById<TextView>(R.id.tv_item_name)
        val subnameHeroes = view.findViewById<TextView>(R.id.tv_item_subname)

        fun bindingview(items: Items, listener: (Items) -> Unit){
            imageHero.setImageResource(items.imageHero)
            nameHeroes.text = items.nameHeroes
            subnameHeroes.text = items.subnameHeroes
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {

        return ItemsViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_hero, parent,false)
        )
    }

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {
        holder.bindingview(items[position], listener)
    }
}